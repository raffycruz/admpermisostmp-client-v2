import { createStore } from 'vuex'
import router from '../router'
import { useUrl } from '../hooks/useUrl'

export default createStore({
  state: {
    permisos: [],
    tipoPermisos: [],
    permiso: { 
      id: 0,
      nombreEmpleado: '',
      apellidosEmpleado: '',
      tipoPermisoId: 0,
      fechaPermiso: '',
      tipoPermiso: {},
    }
  },
  mutations: {
    setTipoPermisos(state, payload){
      state.tipoPermisos = payload;
    }, 
    setPermisos(state, payload){
      state.permisos = payload;
    },   
    setPermisoById(state, payload){
      state.permiso = payload;
    },  
    add(state, payload) {
      router.push('/permisos')
    },
    update(state, payload) {
      state.permisos = state.permisos.map(item => item.id === payload.id ? payload : item);
      router.push('/permisos')
    },
    delete(state, payload) {
      state.permisos = state.permisos.filter(item => item.id !== payload);
      router.push('/permisos');
    },
  },
  actions: {
    async getTipoPermisos({ commit }) {
      try {
        const url = `${useUrl().baseUrl.value}/tipopermiso`;
        const res = await fetch(url);
        const data = await res.json();
        commit('setTipoPermisos', data);
      } catch (error) {
        console.log(error);
      }
    },
    async getPermisos({ commit }) {
      try {
        const url = `${useUrl().baseUrl.value}/permiso`;
        const res = await fetch(url);
        const data = await res.json();
        //console.log('getPermisos', data);
        commit('setPermisos', data);
      } catch (error) {
        console.log(error);
      }
    },
    async getPermisosById({ commit }, id) {
      try {
        const url = `${useUrl().baseUrl.value}/permiso/${id}`;
        const res = await fetch(url);
        const data = await res.json();
        commit('setPermisoById', data);
      } catch (error) {
        console.log(error);
      }
    },   
    async addPermisos({ commit }, permiso) {
      try {
        //console.log('addPermisos', permiso);
        const url = `${useUrl().baseUrl.value}/permiso`;
        const res = await fetch(url, { 
            method: 'POST', headers: { 'Content-Type': 'application/json' }, body: JSON.stringify(permiso)
        });
        const data = await res.json();
        commit('add');
      } catch (error) {
        console.log(error);
      }
    },
    async updatePermisos({ commit }, permiso) {
      try {
        // console.log('updatePermisos', permiso.value)
        const url = `${useUrl().baseUrl.value}/permiso/${permiso.value.id}`;
        const res = await fetch(url, { 
            method: 'PUT', headers: { 'Content-Type': 'application/json' }, body: JSON.stringify(permiso.value)
        });
        // const data = await res.json();
        commit('update', permiso);
      } catch (error) {
        console.log(error);
      }
    },
    async deletePermisos({ commit }, id) {
      try {
        const url = `${useUrl().baseUrl.value}/permiso/${id}`;
        const res = await fetch(url, { 
            method: 'DELETE', headers: { 'Content-Type': 'application/json' }, body: JSON.stringify(id)
        });

        commit('delete', id);
      } catch (error) {
        console.log(error);
      }
    },     
    clearPermiso({ commit }) {
        const permiso =  {
          id: 0,
          nombreEmpleado: '',
          apellidosEmpleado: '',
          tipoPermisoId: 0,
          fechaPermiso: '',
          tipoPermiso: null
        }
        commit('setPermisoById', permiso);
    },

  },
  modules: {
  }
})
