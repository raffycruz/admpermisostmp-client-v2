import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/permisos',
    name: 'Permisos',
    component: () => import(/* webpackChunkName: "permisos" */ '../views/Permisos.vue')
  },
  {
    path: '/permisos/:id',
    name: 'Permiso',
    props: true,
    component: () => import(/* webpackChunkName: "permisos" */ '../views/Permiso.vue')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
