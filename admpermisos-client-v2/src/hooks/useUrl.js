import { onMounted, ref } from 'vue'

export function useUrl(){
    const entorno = ref('dev');
    const baseUrl = ref('');

    const baseUrls = ref({
        dev: 'https://localhost:44308/api',
        prod: 'http://urlproduction.com/api'
    })

    const getUrl = () => {
        baseUrl.value = (entorno.value == 'dev' ? baseUrls.value.dev : baseUrls.value.prod);
    }
    getUrl();

    
    return { baseUrl }
}